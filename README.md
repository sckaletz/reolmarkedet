**INITIALISERING**

Rediger filen App.config ved at ændre connectionstring til at indeholde dine database informationer (Server IP, Database, User Id samt Password).

Kør programmet og klik på "Administrator" og derefter "Initializer database". Dette opretter alle nødvendige tabeller samt keys i din database.


**OPRET REOL**

- Klik på "Administrator"
- Klik på "Opret reol"
- Indtast reoltype
- Klik på "Opret reol"


**OPRET LEJER**

- Klik på enten "Administrator" eller "Lejer"
- Klik på "Opret lejer"
- Indtast alle informationer om lejer
- Klik på "Opret lejer"


**SØG LEJER**

- Klik på "Administrator"
- Klik på "Søg lejer"
- Indtast email på person, du søger
- Klik på "Søg"


**INDTAST VARER**

- Klik på "Administrator" eller "Lejer"
- Klik på "Indtast varer"
- Indtast StandId, Beskrivelse samt Pris
- Klik på "Tilføj produkt"
